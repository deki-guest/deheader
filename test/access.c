/*
 * Items: access(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <unistd.h>

int main(int arg, char **argv)
{
    /* 
     * Note: use of a symbolic constant like F_OK *will* 
     * cause the compile to fail and the requrement
     * for <unistd.h> to be tetected.
     */
    (void)access("/dev/null", 0);
}
