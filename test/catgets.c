/*
 * Items: catgets(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <nl_types.h>

int main(int arg, char **argv)
{
    (void) catgets(0, 0, 0, "foobar");
}
