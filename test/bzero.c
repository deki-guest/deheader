/*
 * Items: bzero(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <strings.h>

int main(int arg, char **argv)
{
    (void) bzero(0, 0);
}
