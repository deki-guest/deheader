/*
 * Items: sbrk(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <unistd.h>

int main(int arg, char **argv)
{
    sbrk(23);
}
