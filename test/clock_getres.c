/*
 * Items: clock_getres(
 * Standardized-By: SuS
 * Detected-by: gcc-4.4.3 + Linux
 */

#include <time.h>

int main(int arg, char **argv)
{
    struct timespec ts;
    (void)clock_getres(0, &ts);
}
