/*
 * Items: chmod(
 * Standardized-By: SuS
 * Not-Detected-by: gcc-4.4.3 + Linux
 */

#include <sys/types.h>
#include <sys/stat.h>

int main(int arg, char **argv)
{
    (void)chmod("/", 0);
}
